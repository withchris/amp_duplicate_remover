
#AMP for Endpoints - Duplicate Fixer

This python script is intended to delete duplicated AMP for EndPoint hostnames.

The script executes as follows: Enter AMP4E -API Credentials -> Search all hostnames -> Create hostname duplicate list -> Query duplicate install date -> delete dated hostname. After the script runs, check AMP4E Console > Accounts > Audit Log, for changes the script performed.

Authors: Max Wijnbladh and Chris Maxwell
